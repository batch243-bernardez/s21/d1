// An array in programming is simply a list of data. Data that are related or connected with each other.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// Now, with an array, we can simply write the code above like this:

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"]



// [Section] Array
/*
	- Arrays are used to store multiple related values in a single variable
	- They are declared using square brackets ([]) also known as "Array Literals"
	- Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks.
	- Array also provide access to a number of function or methods that help achieving specific task
	- "Method" is another term for functions associated with an object or array and is used to execute statements that are relevant.
	- Majority of methods are used to manipulate information stored within the same object
	- Arrays are also object which is another type.

	Syntax:
		let/const arrayName = [elementA, elementB, elementC, ...]
*/ 

// Common examples of Arrays

let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);

let computerBrands = ["Acer", "Asus", "Lenovo", "Dell", "Mac"];
console.log(computerBrands);

// Possible use of an array but is not recommended. 
let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr)

// Alternative way to write arrays
/*let myTasks = ["drink html", "eat javascript", "inhale css"];
console.log(myTasks);
console.log(myTasks[1].length);*/

// Create an arrya with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";

let cities = [city1, city2, city3];

console.log(cities);



// [Section] Length Property
// The .length property allows us to get and set the total number of items in an array.

console.log(typeof grades.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

let array;
console.log(array); /*ans: undefined*/

// Length property can also be used with strings. Some array methods and properties can also be used with strings.

let fullName = "John Doe"; 
console.log(fullName.length); /*ans: 8. space are included to the count*/

// Length property on strings shows the number of characters in a string. Spaces are counted as character in strings.

// Length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.

let myTasks = ["drink html", "eat javascript", "inhale css", "bake sass"];
console.log(myTasks);

myTasks.length = myTasks.length-1;
console.log(myTasks);

// To delete a specific item in array, we can employ array methods (which will be shown in the next session) or an algorithm (set of code to process tasks).

// Example on using decramentation
cities.length--;
console.log(cities);

// We can't do the same on the strings
console.log(fullName.length);
fullName.length = fullName.length-1
console.log(fullName.length);
console.log(fullName);

// If you can shorten array by setting the length of property, you can also lengthen it by adding a number into the length property.
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length = theBeatles.length + 1;
console.log(theBeatles);

theBeatles[4] = "Roda";
console.log(theBeatles);

/*or theBeatles[i+1];*/


// [Section] Reading or Accession Elements of Arrays
// Accessing aaray elements is one of the more common task that we do with an array
// This can be done through the use of index
// Each element in an array is associated iwth its own index or number

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[4]);

// We can also save or store array elements in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// We can also reassign array values using item indices
console.log("Array before the reassignment: ");
console.log(lakersLegends);

lakersLegends[2] = "Pau Gasol";
console.log("Array after the reassignment: ");
console.log(lakersLegends);

// Accessing the last element of an array
// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.
let lastElementIndex = lakersLegends.length-1
console.log(lakersLegends[lastElementIndex]);

// Adding items into the array without using array methods
let newArr = [];
newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length] = "Tifa Lockhart";
console.log(newArr);

// Another example. You can also add at the end of the array. Instead of adding it in the front to avoid the risk of replacing the first items in the array.
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

newArr[newArr.length] = "Chris Mortel"

// Looping over an array
// You can use a for loop to iterate overall items in an array

for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

// We will create a for loop, that will check whether the elements or numbers are divisible by 5.
let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index <= numArr.length-1; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}
	else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}



// [Section] Multi-dimensional array
// Useful for storing complex data structures such as matrix.
// A practical application of this is to help visualize or create real world objects
// Though useful in a number, creating a complex array structure is not always recommended

let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
]

console.table(chessBoard);
console.log(chessBoard[4][5]);
console.log(chessBoard[7][3]);